package calculate_pass_fail;

import java.util.Scanner;

public class PassFail {

	public static void main(String[] args) {
		int math, physics, chemistry, turkish, music, total = 0, count = 0;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter your Math note: ");
		math = scan.nextInt();
		if(!(math < 0 || math > 100)) {
			total += math;
		}else {count++;}
		
		System.out.print("Enter your Physics note: ");
		physics = scan.nextInt();
		if(!(physics < 0 || physics > 100)) {
			total += physics;
		}else {count++;}
		
		System.out.print("Enter your Chemistry note: ");
		chemistry = scan.nextInt();
		if(!(chemistry < 0 || chemistry > 100)) {
			total += chemistry;
		}else {count++;}
		
		System.out.print("Enter your Turkish note: ");
		turkish = scan.nextInt();
		if(!(turkish < 0 || turkish > 100)) {
			total += turkish;
		}else {count++;}
		
		System.out.print("Enter your Music note: ");
		music = scan.nextInt();
		if(!(music < 0 || music > 100)) {
			total += music;
		}else {count++;}
		
		float average = 0;
		if( count < 5) {
			average = total / (5 - count);
			if(average >= 55) {
				System.out.println("You passed!");
				System.out.println(average);
				System.out.println(count);
			}
			else {
				System.out.println("You failed!");
				System.out.println(average);
				System.out.println(count);
			}
		}

	}

}
