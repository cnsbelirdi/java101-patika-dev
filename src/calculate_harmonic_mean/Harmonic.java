package calculate_harmonic_mean;

public class Harmonic {

	public static void main(String[] args) {
		int[] numbers = {1, 2, 3, 4, 5};
		int n = numbers.length;
		double harmonic = 0;
		for(int i = 0; i < n; i++) {
			harmonic += 1.0 / numbers[i];
		}
		
		double total = n / harmonic;
		
		System.out.println("Harmonic Mean : " + total);
			
	}

}
