package calculate_note_avg;

import java.util.Scanner;

public class CalculateAVG {

	public static void main(String[] args) {
		int math, physics, chemistry, turkish, history, music;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter your Math note: ");
		math = scan.nextInt();
		
		System.out.print("Enter your Physics note: ");
		physics = scan.nextInt();
		
		System.out.print("Enter your Chemistry note: ");
		chemistry = scan.nextInt();
		
		System.out.print("Enter your Turkish note: ");
		turkish = scan.nextInt();
		
		System.out.print("Enter your History note: ");
		history = scan.nextInt();
		
		System.out.print("Enter your Music note: ");
		music = scan.nextInt();
		
		float average = (math + physics + chemistry + turkish + history + music) / 6;
		
		System.out.println("Your average is " + average);
	}

}
