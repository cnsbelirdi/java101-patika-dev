package find_biggest;

import java.util.Scanner;

public class Biggest {

	public static void main(String[] args) {
		int max = 1, min = 1;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("How many numbers will you enter : ");
		int n = scan.nextInt();
		
		for(int i = 1; i <= n; i++) {
			System.out.print(i + ". Enter the number : ");
			int x = scan.nextInt();
			
			if(x > max) {
				max = x;
			} else if(x < min) {
				min = x;
			} else {
				min = x;
			}
		}
		System.out.println("Biggest number : " + max);
		System.out.println("Smallest number : " + min);
		
	}

}
