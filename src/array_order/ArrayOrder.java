package array_order;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayOrder {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the array length : ");
		int length = scan.nextInt();
		
		int[] array = new int[length];

		System.out.println("Enter the elements of the array: ");
		for(int i = 0; i < length; i++) {
			System.out.print((i + 1) + ". elements : ");
			array[i] = scan.nextInt();
		}
		
		Arrays.sort(array);
		
		System.out.print("Order : ");
		for(int i : array) {
			System.out.print(i + " ");
		}
	}

}
