package calculate_harmonic;

import java.util.Scanner;

public class Harmonic {

	public static void main(String[] args) {
		double result = 0;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the N number : ");
		int n = scan.nextInt();
		
		for(double i = 1; i <= n; i++) {
			result += (1/i);
		}
		
		System.out.println("Result : " + result);
	}

}
