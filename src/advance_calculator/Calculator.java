package advance_calculator;

import java.util.Scanner;

public class Calculator {

	static void plus() {
        Scanner scan = new Scanner(System.in);
        int number, result = 0, i = 1;
        while (true) {
            System.out.print(i++ + ". number :");
            number = scan.nextInt();
            if (number == 0) {
                break;
            }
            result += number;
        }
        System.out.println("Result : " + result);
    }

    static void minus() {
        Scanner scan = new Scanner(System.in);
        System.out.print("How many numbers will you enter? :");
        int counter = scan.nextInt();
        int number, result = 0;

        for (int i = 1; i <= counter; i++) {
            System.out.print(i + ". number :");
            number = scan.nextInt();
            if (i == 1) {
                result += number;
                continue;
            }
            result -= number;
        }

        System.out.println("Result : " + result);
    }

    static void times() {
        Scanner scan = new Scanner(System.in);
        int number, result = 1, i = 1;

        while (true) {
            System.out.print(i++ + ". number :");
            number = scan.nextInt();

            if (number == 1)
                break;

            if (number == 0) {
                result = 0;
                break;
            }
            result *= number;
        }

        System.out.println("Result : " + result);
    }

    static void divided() {
        Scanner scan = new Scanner(System.in);
        System.out.print("How many numbers will you enter? :");
        int counter = scan.nextInt();
        double number, result = 0.0;

        for (int i = 1; i <= counter; i++) {
            System.out.print(i + ". number :");
            number = scan.nextDouble();
            if (i != 1 && number == 0) {
                System.out.println("You cannot enter the divisor 0.");
                continue;
            }
            if (i == 1) {
                result = number;
                continue;
            }
            result /= number;
        }

        System.out.println("Result : " + result);
    }

    static void power() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter number :");
        int base = scan.nextInt();
        System.out.print("Enter power value :");
        int exponent = scan.nextInt();
        int result = 1;

        for (int i = 1; i <= exponent; i++) {
            result *= base;
        }

        System.out.println("Result : " + result);
    }

    static void factorial() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter number :");
        int n = scan.nextInt();
        int result = 1;

        for (int i = 1; i <= n; i++) {
            result *= i;
        }

        System.out.println("Result : " + result);
    }


    static void mod() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter dividend :");
        int dividend = scan.nextInt();
        System.out.print("Enter divisor :");
        int divisor = scan.nextInt();
        
        int result = dividend % divisor;

        System.out.println("Result : " + result);
    }
    
    static void rectangle() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter long edge :");
        int a = scan.nextInt();
        System.out.print("Enter short edge :");
        int b = scan.nextInt();
        
        int area = a * b;
        
        int perimeter = (2 * a) + (2 * b);

        System.out.println("Area : " + area + "\nPerimeter : " + perimeter);
    }
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int select;
        String menu = "1- Addition Operation\n"
                + "2- Subtraction\n"
                + "3- Multiplication\n"
                + "4- Division operation\n"
                + "5- Exponent Calculation\n"
                + "6- Factorial Calculation\n"
                + "7- Get Mod\n"
                + "8- Rectangular Area and Perimeter Calculation\n"
                + "0- Sign Out";

        do {
            System.out.println(menu);
            System.out.print("Please select an action :");
            select = scan.nextInt();
            switch (select) {
                case 1:
                    plus();
                    break;
                case 2:
                    minus();
                    break;
                case 3:
                    times();
                    break;
                case 4:
                    divided();
                    break;
                case 5:
                    power();
                    break;
                case 6:
                    factorial();
                    break;
                case 7:
                	mod();
                	break;
                case 8:
                	rectangle();
                	break;
                case 0:
                    break;
                default:
                    System.out.println("You entered an incorrect value, try again.");
            }
        } while (select != 0);


    }

}
