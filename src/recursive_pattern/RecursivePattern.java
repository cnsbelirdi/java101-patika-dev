package recursive_pattern;

import java.util.Scanner;

public class RecursivePattern {
	
	public static void createPattern(int number, int temp, int result) {
		if(temp > 0) {
			System.out.print(result + " ");
			result -= 5;
			
			if(result <= 0)
				temp = result;
		}else if(temp <= 0) {
			System.out.print(result + " ");
			result += 5;
			
			if(result == number) {
				System.out.print(result + " ");
				return;
			}
		}
		
		createPattern(number, temp, result);
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("N number : ");
		int number = scan.nextInt();
		
		createPattern(number, number, number);
	}

}
