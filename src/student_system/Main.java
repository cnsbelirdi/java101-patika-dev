package student_system;

public class Main {
    public static void main(String[] args) {

        Course math = new Course("Math", "MATH101", "MATH", 30);
        Course physics = new Course("Physics", "PHY101", "PHY", 40);
        Course chemistry = new Course("Chemistry", "CHEM101", "CHEM", 40);

        Teacher t1 = new Teacher("Mahmut Hoca", "90550000000", "MATH");
        Teacher t2 = new Teacher("Fatma Ay�e", "90550000001", "PHY");
        Teacher t3 = new Teacher("Ali Veli", "90550000002", "CHEM");

        math.addTeacher(t1);
        physics.addTeacher(t2);
        chemistry.addTeacher(t3);

        Student s1 = new Student("�nek �aban", 4, "140144015", math, physics, chemistry);
        s1.addVerbalGrade(60,30,50);
        s1.addExamGrade(50,20,40);
        s1.isPass();

        Student s2 = new Student("G�d�k Necmi", 4, "2211133", math, physics, chemistry);
        s2.addVerbalGrade(90,60,50);
        s2.addExamGrade(100,50,40);
        s2.isPass();

        Student s3 = new Student("Hayta �smail", 4, "221121312", math, physics, chemistry);
        s3.addVerbalGrade(70,30,50);
        s3.addExamGrade(60,40,70);
        s3.isPass();

    }
}
