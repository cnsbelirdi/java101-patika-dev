package student_system;

public class Student {
    String name,stuNo;
    int classes;
    Course math;
    Course physics;
    Course chemistry;
    double average;
    boolean isPass;


    Student(String name, int classes, String stuNo, Course math,Course physics,Course chemistry) {
        this.name = name;
        this.classes = classes;
        this.stuNo = stuNo;
        this.math = math;
        this.physics = physics;
        this.chemistry = chemistry;
        calcAverage();
        this.isPass = false;
    }

    
    public void addVerbalGrade(int math, int physics, int chemistry) {

        if (math >= 0 && math <= 100) {
            this.math.verbal = math;
        }

        if (physics >= 0 && physics <= 100) {
            this.physics.verbal = physics;
        }

        if (chemistry >= 0 && chemistry <= 100) {
            this.chemistry.verbal = chemistry;
        }

    }

    public void addExamGrade(int math, int physics, int chemistry) {

        if (math >= 0 && math <= 100) {
            this.math.exam = math;
        }

        if (physics >= 0 && physics <= 100) {
            this.physics.exam = physics;
        }

        if (chemistry >= 0 && chemistry <= 100) {
            this.chemistry.exam = chemistry;
        }

    }

    public void isPass() {
        if (this.math.exam == 0 || this.physics.exam == 0 || this.chemistry.exam == 0 || this.math.verbal == 0 || this.physics.verbal == 0 || this.chemistry.verbal == 0) {
            System.out.println("Some grades are undefined.");
        } else {
            this.isPass = isCheckPass();
            printgrade();
            System.out.println("Average : " + this.average);
            if (this.isPass) {
                System.out.println("PASS!");
            } else {
                System.out.println("FAIL!");
            }
        }
    }

    public void calcAverage() {
    	double math_avg = (this.math.verbal * this.math.verbalPercent) + (this.math.exam * (1 - this.math.verbalPercent));
    	double phy_avg = (this.physics.verbal * this.physics.verbalPercent) + (this.physics.exam * (1 - this.physics.verbalPercent));
    	double chem_avg = (this.chemistry.verbal * this.chemistry.verbalPercent) + (this.chemistry.exam * (1 - this.chemistry.verbalPercent));
        double _average = (math_avg + phy_avg + chem_avg) / 3.0;
        
        this.average = Math.round(_average*100)/100d;
    }

    public boolean isCheckPass() {
        calcAverage();
        return this.average > 55;
    }

    public void printgrade(){
        System.out.println("====================================================");
        System.out.println("Student : " + this.name);
        System.out.println("\nMath -> Verbal Grade : " + this.math.verbal + " / Exam Grade : " + this.math.exam);
        System.out.println("Math -> Verbal Percent : " + this.math.verbalPercent * 100 + "% / Exam Percent : " + (1 - this.math.verbalPercent) * 100 + "%\n");
        System.out.println("Physics -> Verbal Grade : " + this.physics.verbal + " / Exam Grade : " + this.physics.exam);
        System.out.println("Physics -> Verbal Percent : " + this.physics.verbalPercent * 100 + "% / Exam Percent : " + (1 - this.physics.verbalPercent) * 100 + "%\n");
        System.out.println("Chemistry -> Verbal Grade : " + this.chemistry.verbal + " / Exam Grade : " + this.chemistry.exam);
        System.out.println("Chemistry -> Verbal Percent : " + this.chemistry.verbalPercent * 100 + "% / Exam Percent : " + (1 - this.chemistry.verbalPercent) * 100 + "%\n");
    }

}
