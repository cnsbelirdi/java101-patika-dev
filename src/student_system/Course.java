package student_system;

public class Course {
    Teacher courseTeacher;
    String name;
    String code;
    String prefix;
    int exam, verbal;
    double verbalPercent;

    public Course(String name, String code, String prefix, int verbalPercent) {
        this.name = name;
        this.code = code;
        this.prefix = prefix;
        this.exam = 0;
        this.verbal = 0;
        this.verbalPercent = verbalPercent / 100.0;
    }

    public void addTeacher(Teacher t) {
        if (this.prefix.equals(t.branch)) {
            this.courseTeacher = t;
            System.out.println("Successfully completed.");
        } else {
            System.out.println(t.name + " cannot teach this lesson.");
        }
    }

    public void printTeacher() {
        if (courseTeacher != null) {
            System.out.println("Teacher of the "+ this.name + " course : " + courseTeacher.name);
        } else {
            System.out.println(this.name + " course has not a teacher yet.");
        }
    }
}
