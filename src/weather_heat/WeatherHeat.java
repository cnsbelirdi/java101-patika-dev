package weather_heat;

import java.util.Scanner;

public class WeatherHeat {

	public static void main(String[] args) {
		float heat;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Type the weather heat : ");
		heat = scan.nextFloat();
		
		if(heat < 5) {
			System.out.println("Ski!");
		}
		else if(heat >= 5 && heat <= 15 ){
			System.out.println("Cinema!");
		}
		else if( heat >= 15 && heat <= 25) {
			System.out.println("Picnic!");
		}
		else{
			System.out.println("Swim!");
		}
	}

}
