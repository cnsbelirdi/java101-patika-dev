package find_perfect;

import java.util.Scanner;

public class PerfectNumber {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		while(true) {
			
			System.out.print("Enter a number : ");
			int number = scan.nextInt();
			
			if(number == 0) {
				break;
			}
			
			int total = 0;
			
			for(int i = 1; i < number; i++) {
				if(number % i == 0) {
					total += i;
				}
			}
			
			if(total == number) {
				System.out.println(number + " is a perfect number!");
			}else {
				System.out.println(number + " is not a perfect number!");
			}
			System.out.println("\n****For exit enter 0****\n");
		}
	}

}
