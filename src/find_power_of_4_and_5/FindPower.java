package find_power_of_4_and_5;

import java.util.Scanner;

public class FindPower {

	public static void main(String[] args) {
		int n; 
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Type a number : ");
		n = scan.nextInt();
		
		System.out.println(1);
		for(int i = 1; i <= n; i++) {
			if(i % 4 == 0 || i % 5 == 0) {
				System.out.println(i);
			}
		}
	}

}
