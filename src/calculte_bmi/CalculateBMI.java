package calculte_bmi;

import java.util.Scanner;

public class CalculateBMI {

	public static void main(String[] args) {
		double weight, height;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter your weight : ");
		weight = scan.nextDouble();
		
		System.out.print("Enter your height : ");
		height = scan.nextDouble();
		
		double bmi = weight / (height * height);
		
		System.out.println("Your Body Mass Index : " + bmi);
		
	}

}
