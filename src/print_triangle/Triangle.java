package print_triangle;

import java.util.Scanner;

public class Triangle {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the number of digits : ");
		int digit = scan.nextInt();
		
		for(int i = digit; i >= 1; i--) {
			
			for(int x = digit - i; x > 0; x--) {
				System.out.print(" ");
			}
			
			for(int j = (i * 2) - 1; j >= 1; j--) {
				System.out.print("*");
			}
			System.out.println("");
		}
	}

}
