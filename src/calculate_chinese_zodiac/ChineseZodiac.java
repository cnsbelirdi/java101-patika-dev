package calculate_chinese_zodiac;

import java.util.Scanner;

public class ChineseZodiac {

	public static void main(String[] args) {
		int year;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Type your birth year : ");
		year = scan.nextInt();
		
		switch(year % 12) {
			case 0:
				System.out.println("Your Chinese Zodiac's Horoscope : Horse");
				break;
			case 1:
				System.out.println("Your Chinese Zodiac's Horoscope : Rooster");
				break;
			case 2:
				System.out.println("Your Chinese Zodiac's Horoscope : Dog");
				break;
			case 3:
				System.out.println("Your Chinese Zodiac's Horoscope : Pig");
				break;
			case 4:
				System.out.println("Your Chinese Zodiac's Horoscope : Rat");
				break;
			case 5:
				System.out.println("Your Chinese Zodiac's Horoscope : Ox");
				break;
			case 6:
				System.out.println("Your Chinese Zodiac's Horoscope : Tiger");
				break;
			case 7:
				System.out.println("Your Chinese Zodiac's Horoscope : Rabbit");
				break;
			case 8:
				System.out.println("Your Chinese Zodiac's Horoscope : Dragon");
				break;
			case 9:
				System.out.println("Your Chinese Zodiac's Horoscope : Snake");
				break;
			case 10:
				System.out.println("Your Chinese Zodiac's Horoscope : Horse");
				break;
			case 11:
				System.out.println("Your Chinese Zodiac's Horoscope : Goat");
				break;
		}
		
	}

}
