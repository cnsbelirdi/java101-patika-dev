package find_horoscope;

import java.util.Scanner;

public class Horoscope {

	public static void main(String[] args) {
		int month, day;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter your birth day (1-31) : ");
		day = scan.nextInt();
		
		System.out.print("Enter your birth month (1-12) : ");
		month = scan.nextInt();

		
		if((day > 31 || day < 1) || (month > 12 || month < 1)) {
			System.out.println("INVALID!");
		}else {
			if((month == 3 && day > 20) || (month == 4 && day < 21)) {
				System.out.println("Aries!");
			}
			else if((month == 4 && day > 20) || (month == 5 && day < 22)) {
				System.out.println("Taurus!");
			}
			else if((month == 5 && day > 21) || (month == 6 && day < 23)) {
				System.out.println("Gemini!");
			}
			else if((month == 6 && day > 22) || (month == 7 && day < 23)) {
				System.out.println("Cancer!");
			}
			else if((month == 7 && day > 22) || (month == 8 && day < 23)) {
				System.out.println("Leo!");
			}
			else if((month == 8 && day > 22) || (month == 9 && day < 23)) {
				System.out.println("Virgo!");
			}
			else if((month == 9 && day > 22) || (month == 10 && day < 23)) {
				System.out.println("Libra!");
			}
			else if((month == 10 && day > 22) || (month == 11 && day < 22)) {
				System.out.println("Scorpio!");
			}
			else if((month == 11 && day > 21) || (month == 12 && day < 22)) {
				System.out.println("Sagittarius!");
			}
			else if((month == 12 && day > 21) || (month == 1 && day < 22)) {
				System.out.println("Capricorn!");
			}
			else if((month == 1 && day > 21) || (month == 2 && day < 20)) {
				System.out.println("Aquarius!");
			}
			else if((month == 2 && day > 19) || (month == 3 && day < 21)) {
				System.out.println("Pisces!");
			}
		}
		
	}

}
