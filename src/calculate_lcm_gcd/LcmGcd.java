package calculate_lcm_gcd;

import java.util.Scanner;

public class LcmGcd {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter first number : ");
		int n1 = scan.nextInt();

		System.out.print("Enter second number : ");
		int n2 = scan.nextInt();
		
		if(n2 < n1) {
			int temp = n1;
			n1 = n2;
			n2 = temp;
		}
		
		int i = n1;
		while(i >= 1) {
			if(n1 % i == 0 && n2 % i == 0) {
				System.out.println("GCD : " + i);
				break;
			}else {
				i--;
			}
			
		}
		
		int j = n2;
		while(j <= (n1 * n2)) {
			if(j % n1 == 0 && j % n2 == 0) {
				System.out.println("LCM : " + j);
				break;
			}else {
				j++;
			}
		}
		
		
	}

}
