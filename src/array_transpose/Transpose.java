package array_transpose;

public class Transpose {

	public static void main(String[] args) {
		int[][] matrice = {{1,2,3},{4,5,6}};
		
		int[][] transpose = new int[3][2];
		
		/* Print matrice */
		System.out.println("Matrice :");
		for(int i = 0; i < 2; i++) {
			for(int j = 0; j < 3; j++) {
				System.out.print(matrice[i][j] + " ");
			}
			System.out.println();
		}
		
		
		for(int i = 0; i < 2; i++) {
			for(int j = 0; j < 3; j++) {
				transpose[j][i] = matrice[i][j];
			}
		}
		
		/* Print transpose */
		System.out.println("Transpose : ");
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 2; j++) {
				System.out.print(transpose[i][j] + " ");
			}
			System.out.println();
		}
	}

}
