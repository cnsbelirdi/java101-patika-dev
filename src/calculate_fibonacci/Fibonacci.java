package calculate_fibonacci;

import java.util.Scanner;

public class Fibonacci {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the number of elements : ");
		int n = scan.nextInt();
		int n1 = 0,n2 = 1,n3;
		for(int i = 0; i < n; i++) {
			n3 = n1 + n2;
			System.out.println(n1 + " + " + n2 + " = " + n3);
			n1 = n2;
			n2 = n3;
		}
	}
}
