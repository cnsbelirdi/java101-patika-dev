package calculate_tax;
import java.util.Scanner;

public class CalculateTAX {

	public static void main(String[] args) {
		double price, taxRate;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the price: ");
		price = scan.nextDouble();
		
		if(price > 0 && price <= 1000) taxRate = 0.18;
		else taxRate = 0.8;
		
		double taxPrice = price * taxRate;
		double lastPrice = price + taxPrice;
		
		System.out.println("Price : " + price);
		System.out.println("Price with tax : " + lastPrice);
		System.out.println("Tax price : " + taxPrice);
		
	}

}
