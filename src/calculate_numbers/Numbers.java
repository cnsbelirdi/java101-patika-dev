package calculate_numbers;

import java.util.Scanner;

public class Numbers {

	public static void main(String[] args) {
		int n, total = 0, count = 0;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Type a number : ");
		n = scan.nextInt();
		
		for(int i = 1; i <= n; i++) {
			if(i % 3 == 0  && i % 4 == 0) {
				System.out.println(i);
				total += i;
				count++;
			}
		}
		
		System.out.println("The average of  0 to "+ n + " numbers which is divided by 3 and 4 : " + total/count);
	}

}
