package find_prime_recursive;

import java.util.Scanner;

public class RecursivePrime {
	
	public static int isPrime(int number, int i) {
		if(i==1) 
			return 1;
		else {
			if(number % i == 0) 
				return 0;
			else
				return isPrime(number, i-1);
		}
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the number : ");
		int number = scan.nextInt();
		
		if(isPrime(number, number/2) == 1) {
			System.out.println(number + " is Prime!");
		}
		else
			System.out.println(number + " is NOT Prime!");
	}

}
