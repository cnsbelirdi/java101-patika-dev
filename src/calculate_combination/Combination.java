package calculate_combination;

import java.util.Scanner;

public class Combination {

	public static void main(String[] args) {
		int n, r, combination;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the number of elements of the set (n) : ");
		n = scan.nextInt();
		
		System.out.print("Enter the number of different groups (r) : ");
		r = scan.nextInt();
		
		combination = factorial(n) / (factorial(r) * factorial(n-r));
		
		System.out.println("C("+n+","+r+") = " + combination);
		
	}

	public static int factorial(int x) {
		int fact = 1;
		for(int i = 1; i <= x; i++) {
			fact *= i;
		}
		return fact;
	}
}
