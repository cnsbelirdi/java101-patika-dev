package calculate_sum_of_digits;

import java.util.Scanner;

public class SumDigits {

	public static void main(String[] args) {
		int number, sum = 0;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the number : ");
		number = scan.nextInt();
		
		while(number != 0) {
			sum += number % 10;
			number /= 10;
		}
		
		System.out.println("Sum of digits : " + sum);
	}

}
