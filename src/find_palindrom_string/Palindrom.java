package find_palindrom_string;

import java.util.Scanner;

public class Palindrom {
	
	static boolean isPalindrome(String str) {
        String reverse = "";
        for (int i = str.length() - 1; i >= 0; i--) {
            reverse += str.charAt(i);
        }
        if (str.equals(reverse))
            return true;
        else
            return false;
    }

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the string : ");
		
		String str = scan.nextLine();
		
		System.out.println("Is the " + str + " Palindrome?");
		if(isPalindrome(str)) 
			System.out.println("YES");
		else 
			System.out.println("NO");
		
		
	}
	
}
