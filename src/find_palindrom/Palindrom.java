package find_palindrom;

public class Palindrom {

	public static void isPalindrom(int number) {
		int temp = number, reverse = 0, last;
		
		while(temp != 0) {
			last = temp % 10;
			reverse = (reverse * 10) + last;
			temp /= 10;
		}
		
		if(reverse == number) {
			System.out.println(number + " is Palindrom");
		}else {
			System.out.println(number + " is not Palindrom");
		}
	}
	
	public static void main(String[] args) {
		isPalindrom(96769);
	}

}
