package calculate_hypo;

import java.util.Scanner;

public class CalculateHypo {

	public static void main(String[] args) {
		int a,b;
		double c;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter your first edge : ");
		a = scan.nextInt();
		
		System.out.print("Enter your second edge : ");
		b = scan.nextInt();
		
		c = Math.sqrt((a * a) + (b * b));
		
		System.out.println("Hypotenus : " + c);
	}

}
