package find_until_odd;

import java.util.Scanner;

public class OddEven {

	public static void main(String[] args) {
		int total = 0,n = 0;

		Scanner scan = new Scanner(System.in);
		do {
			System.out.print("Type a number : ");
			n = scan.nextInt();
			if(n % 4 == 0) {
				total += n;
			}
		}while(n % 2 == 0);
		
		System.out.println("The total of numbers divided by 4 : " + total);
	}

}
