package sort_numbers;

import java.util.Scanner;

public class SortNumbers {

	public static void main(String[] args) {
		int x,y,z;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("1. number : ");
		x = scan.nextInt();

		System.out.print("2. number : ");
		y = scan.nextInt();

		System.out.print("3. number : ");
		z = scan.nextInt();
		
		if(x > y && x > z) {
			if(y > z) {
				System.out.println( x + " > " + y + " > " + z );
			}else {
				System.out.println( x + " > " + z + " > " + y );
			}
		}
		else if ( y > x && y > z) {
			if(x > z) {
				System.out.println( y + " > " + x + " > " + z );
			}else{
				System.out.println( y + " > " + x + " > " + z );
			}
		}
		else if(z > x && z > y) {
			if(x > y) {
				System.out.println( z + " > " + x + " > " + y );
			}else{
				System.out.println( z + " > " + y + " > " + x );
			}
		}
		else {
			System.out.println( x + " = " + y + " = " + z );
		}
	}

}
