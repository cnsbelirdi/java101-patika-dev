package sign_in;

import java.util.Scanner;

public class SignIn {

	public static void main(String[] args) {
		String defUName = "admin", defPword = "test";
		
		String username, temp, password;
		
		Scanner scan = new Scanner(System.in);
		Scanner scan2 = new Scanner(System.in);
		
		System.out.print("Your username : ");
		username = scan.nextLine();
		
		System.out.print("Your password : ");
		password = scan.nextLine();
		
		if(!(defUName.equals(username) && defPword.equals(password))) {
			System.out.println("Your username or password is wrong!\n");
			int select;
			System.out.print("1:Username - 2:Password - 3:Exit\nDo you want to change your username or password?(Type number): ");
			select = scan.nextInt();
			
			switch(select) {
				case 1:
					while(true) {
						System.out.print("\nType your new username: ");
						temp = scan2.nextLine();
						if(!defUName.equals(temp)) {
							System.out.println("Username was changed successfully!");
							defUName = temp;
							break;
						}
						else {
							System.out.println("Username cannot change, try again!");
						}
					}
					break;
				case 2:
					while(true) {
						System.out.print("Type your new password: ");
						temp = scan2.nextLine();
						if(!defPword.equals(temp)) {
							System.out.println("Password was changed successfully!");
							defPword = temp;
							break;
						}
						else {
							System.out.println("Password cannot change, try again!\n");
						}
					}
					break;
				case 3:
					System.out.println("\nSign in terminated.");
					break;
			}
			
		}else {
			System.out.println("Sign in successfull!");
		}
		
		

	}

}
