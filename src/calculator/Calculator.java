package calculator;

import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		int n1, n2, select;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter first number: ");
		n1 = scan.nextInt();
		System.out.print("Enter second number: ");
		n2 = scan.nextInt();
		
		System.out.print("1-Addition\n2-Subtraction\n3-Multiplication\n4-Division\nSelect your process : ");
		select = scan.nextInt();
		
		switch(select) {
			case 1:
				System.out.println("Addition : " + (n1 + n2));
				break;
			case 2:
				System.out.println("Substraction : " + (n1 - n2));
				break;
			case 3:
				System.err.println("Multiplication : " + (n1 * n2));
				break;
			case 4:
				System.out.println("Division : " + (n1 / n2));
				break;
			default:
				System.out.println("Invalid process");
				break;
		}
		
		
	}

}
