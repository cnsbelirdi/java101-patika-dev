package mine_sweeper_game;

import java.util.Arrays;
import java.util.Scanner;

public class MineSweeper {
	private int row, column, mineCount;
	
	private String[][] mineMap;
	
	private String[][] gameMap;
	
	private boolean gameON;

	public MineSweeper(int row, int column) {
		this.row = row;
		this.column = column;
		this.mineCount = (row * column) / 4;
		this.gameON = true;
		createMineMap();
		createGameMap();
	}
	
	/* Creates mine map */
	private void createMineMap(){
		String[][] array = new String[this.row][this.column];
		
		/* Creates random mine locations */
		for(int i = 0; i < this.mineCount; i++) {
			int rnd_Row = (int) (Math.random() * this.row);
			int rnd_Column = (int) (Math.random() * this.column);
			
			array[rnd_Row][rnd_Column] = "*";
		}
		
		for(int i = 0; i < this.row; i++) {
			for(int j = 0; j < this.column; j++) {
				if(array[i][j] == null) {
					array[i][j] = "-";
				}
			}
		}
		this.mineMap = array.clone();
	}
	
	/* Creates standard game map */
	private void createGameMap() {
		String[][] array = new String[this.row][this.column];
		for(int i = 0; i < this.row; i++) {
			for(int j = 0; j < this.column; j++) {
				array[i][j] = "-";
			}
		}
		this.gameMap = array.clone();
	}
	
	/* Gets game standard map */
	public void printMap(String arr[][]) {
		System.out.println("=========================");
		for(int i = 0; i < this.row; i++) {
			for(int j = 0; j < this.column; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}

	/* Play game method */
	public void playGame() {
		System.out.println("Total mine : " + mineCount);
		Scanner scan = new Scanner(System.in);
		printMap(this.gameMap);
		while(this.gameON) {
			System.out.print("Enter row : ");
			int _row = scan.nextInt();
			
			System.out.print("Enter column : ");
			int _column = scan.nextInt();
			
			if((_row  > this.row || _row <= 0) || (_column > this.column || _column <= 0)) {
				System.out.println("Values are not within map boundaries. Enter again!");
				continue;
			}
			
			checkMap(_row-1, _column-1);
			
			if(isWin()) {
				System.out.println("YOU WIN! CONGRATS!!");
			}
			
		}
	}
	
	/* Checks mines */
	private void checkMap(int _row, int _column) {
		
		if(this.mineMap[_row][_column].equals("*")) {
			this.gameMap[_row][_column] = "*";
			printMap(this.gameMap);
			System.out.println("BOOM!!\nThe game is OVER!");
			this.gameON = false;
			System.out.println("Mine Locations : ");
			printMap(this.mineMap);
		}
		else {
			int count = 0;
			for (int i = (_row - 1); i <= (_row + 1); i++) {
	            for (int j = (_column - 1); j <= (_column + 1); j++) {
	                if (i < 0 || j < 0 || i >= this.row || j >= this.column) {
	                    continue;
	                }
	                if (this.mineMap[i][j].equals("*")) {
	                	count++;
	                }
	            }
	        }
			this.gameMap[_row][_column] = String.valueOf(count);
			printMap(this.gameMap);
		}
	}

	/* Checks game is over? */
	private boolean isWin() {
		int count = 0;
		for(int i = 0; i < this.row; i++) {
			for(int j = 0; j < this.column; j++) {
				if(gameMap[i][j].equals("-")) {
					count++;
				}
			}
		}
		if(count > mineCount) {
			return false;
		}
		this.gameON = false;
		return true;
	}
}
