package mine_sweeper_game;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Welcome to Minesweeper Game !");
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter row : ");
		int row = scan.nextInt();
		
		System.out.print("Enter column : ");
		int column = scan.nextInt();
		
		MineSweeper m1 = new MineSweeper(row, column);
		m1.playGame();
		
	}

}
