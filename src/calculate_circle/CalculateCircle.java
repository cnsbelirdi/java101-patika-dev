package calculate_circle;

import java.util.Scanner;

public class CalculateCircle {

	public static void main(String[] args) {
		int r, angle;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the radius : ");
		r = scan.nextInt();
		
		System.out.print("Enter the angle : ");
		angle = scan.nextInt();
		
		double area = Math.PI * r * r;
		
		double perimeter = 2 * Math.PI * r;
		
		double pieArea = (area * angle) / 360;
		
		System.out.println("The area : " + area);
		System.out.println("The perimeter : " + perimeter);
		System.out.println("The pie area : " + pieArea);
		
	}

}
