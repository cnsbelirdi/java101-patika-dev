package calculate_salary;

public class Employee {
    private String name;
    private double salary;
    private int workHours;
    private int hireYear;

    public Employee(String name, double salary, int workHours, int hireYear){
        this.name = name;
        this.salary = salary;
        this.workHours = workHours;
        this.hireYear = hireYear;
    }

    public double tax(){
        if(this.salary > 1000){
            return this.salary * 0.03;
        }
        return 0;
    }

    public double bonus(){
        if(this.workHours > 40){
            return (this.workHours - 40) * 30;
        }
        return 0;
    }

    public double raiseSalary(){
        int year = 2021 - this.hireYear;
        double raise = 0;
        if(year < 10){
            raise = this.salary * 0.05;
        }
        if( year > 9 && year < 20){
            raise = this.salary * 0.1;
        }
        if( year > 19){
            raise  = this.salary * 0.15;
        }
        return raise;
    }

    @Override
    public String toString(){
    	double total = this.salary - tax() + bonus() + raiseSalary();
    	double tax = tax();
    	double bonus = bonus();
    	double raise = raiseSalary();
        System.out.println("-----EMPLOYEE-----");
        System.out.println("Name : " + this.name);
        System.out.println("Salary : " + this.salary);
        System.out.println("Working Hours : " + this.workHours);
        System.out.println("Hired Year : " + this.hireYear);
        System.out.println("Tax : " + tax());
       	System.out.println("Bonus : " + bonus());
        System.out.println("Raise Amount : " + raiseSalary());
        this.salary += bonus - tax;
        System.out.println("New Salary With Tax and Bonus : " + this.salary);
        this.salary += raise;
        System.out.println("Total Salary : " + this.salary);
        return null;
    }
}
