package number_guessing_game;

import java.util.Arrays;
import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        int number = (int) (Math.random() * 100);

        int right = 0;
        int selected;
        int[] wrong = new int[5];
        boolean isWin = false;
        boolean isWrong = false;

        Scanner scan = new Scanner(System.in);
        
        System.out.println("-------NUMBER GUESSING GAME-------");
        
        while (right < 5) {
            System.out.print("\nPlease enter your guess : ");
            selected = scan.nextInt();

            if (selected < 0 || selected > 99) {
                System.out.println("Please enter a value between 0-100.");
                if (isWrong) {
                    right++;
                    System.out.println("You made too many incorrect entries. Remaining right: " + (5 - right));
                } else {
                    isWrong = true;
                    System.out.println("Your next wrong entry will be deducted from your rights.");
                }
                continue;
            }

            if (selected == number) {
                System.out.println("Congratulations, correct guess! The number you guessed : " + number);
                isWin = true;
                break;
            } else {
                System.out.println("\nYou entered an incorrect number!\n");
                if (selected > number) {
                    System.out.println(selected + " number is greater than the hidden number.\n");
                } else {
                    System.out.println(selected + " number is smaller than the hidden number.\n");
                }

                wrong[right++] = selected;
                System.out.println("Remaining right: " + (5 - right) + "\n");
                System.out.println("---------------------------------------");
            }
        }

        if (!isWin) {
            System.out.println("\nYou lost! ");
            if (!isWrong) {
                System.out.println("Your guesses : " + Arrays.toString(wrong));
            }
            System.out.println("Hidden number : " + number);
        }

    }
}
