package calculate_taximeter;

import java.util.Scanner;

public class CalculateTaxiMeter {

	public static void main(String[] args) {
		double price = 10;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Write the total kilometer : ");
		double km = scan.nextDouble();
		
		price += (km * 2.20);
		
		if(price <= 20) {
			System.out.println("Total price : 20TL");
		}
		else {
			System.out.println("Total price : " + price + "TL");
		}
	}

}
