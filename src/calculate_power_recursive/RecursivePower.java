package calculate_power_recursive;

import java.util.Scanner;

public class RecursivePower {
	
	public static int getPower(int number, int power) {
		if(power == 0) 
			return 1;
		else if(number == 1) 
			return 1;
		else {
			return number * getPower(number, power-1);
		}
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the number : ");
		int number = scan.nextInt();

		System.out.print("Enter the power : ");
		int power = scan.nextInt();
		
		System.out.println(getPower(number, power));
	}

}
