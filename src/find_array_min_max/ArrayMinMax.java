package find_array_min_max;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayMinMax {

	public static void main(String[] args) {
		int[] list = {56, 34, 1, 8, 101, -2, -33};
	    Arrays.sort(list);
	    System.out.println("List: " + Arrays.toString(list));
	    
        int min = list[0];
        int max = list[0];
        
        for (int i : list) {
            if (i < min) {
                min = i;
            }
            if (i > max) {
                max = i;
            }
        }
        
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Enter the number : ");
        int number = scan.nextInt();
        
        if(number < min) {
        	System.out.println("There is no number in the array less than entered number.");
        	System.out.println("Nearest maximum number : " + min);
        }
        if(number > max) {
        	System.out.println("Nearest minimum number : " + max);
        	System.out.println("There is no number in the array bigger than entered number.");
        }
        if(number >= list[0]) {
        	for(int i = 0; i < list.length; i++) {
        		if (number == min) {
        			System.out.println("There is no number in the array less than entered number.");
                    System.out.println("Nearest maximum number : " + list[1]);
                    break;
                }
                if (number == max) {
                    System.out.println("Nearest minimum number : " + list[list.length - 2]);
                    System.out.println("There is no number in the array bigger than entered number.");
                    break;
                }
                if (number < list[i]) {
                    System.out.println("Nearest minimum number : " + list[i - 1]);
                    System.out.println("Nearest maximum number : " + list[i]);
                    break;
                }
        	}
        }
        
	}

}
