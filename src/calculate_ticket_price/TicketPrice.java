package calculate_ticket_price;

import java.util.Scanner;

public class TicketPrice {

	public static void main(String[] args) {
		int age,km, flightType;
		double price = 0;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("------CALCULATE TICKET PRICE------\n");
		
		while (true) {
			System.out.print("Enter your distance (km) : ");
			km = scan.nextInt();			
			if(km < 0) {
				System.out.println("You typed invalid information. Please try again!\n");
				continue;
			}
			
			System.out.print("Enter your age : ");
			age = scan.nextInt();
			if(age < 0) {
				System.out.println("You typed invalid information. Please try again!\n");
				continue;
			}

			System.out.print("Enter your flight type (1 : One way / 2: Round trip) : ");
			flightType = scan.nextInt();
			if(flightType != 2 && flightType != 1) {
				System.out.println("You typed invalid information. Please try again!\n");
				continue;
			}
			
			price += km * 0.1;
			
			break;
		}
		
		if(age < 12) {
			price = price * 0.5;
		}else if(age >= 12 && age <=24) {
			price = price * 0.9;
		}else if(age >= 65) {
			price = price * 0.7;
		}
		
		if(flightType == 2) {
			price = price * 0.8;
			price *= 2;
		}
		
		System.out.println("\nTotal price : " + price + "TL");
	}

}
