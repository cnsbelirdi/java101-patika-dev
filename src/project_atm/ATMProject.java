package project_atm;

import java.util.Scanner;

public class ATMProject {

	public static void main(String[] args) {
		String userName, password;
        Scanner input = new Scanner(System.in);
        int right = 3;
        int balance = 1500;
        int select,price;
        while (right > 0) {
            System.out.print("Username :");
            userName = input.nextLine();
            System.out.print("Password : ");
            password = input.nextLine();

            if (userName.equals("test") && password.equals("1234")) {
                System.out.println("Hello, Welcome to Bank!");
                do {
                    System.out.println("1-Deposit\n" +
                            "2-Withdraw\n" +
                            "3-Balance Check\n" +
                            "4-Log out");
                    System.out.print("Please select the action you want to do : ");
                    select = input.nextInt();
                    switch(select) {
                    	case 1:
                    		System.out.print("Amount of money : ");
                            price = input.nextInt();
                            balance += price;
                    		break;
                    	case 2:
                    		System.out.print("Amount of money : ");
                            price = input.nextInt();
                            if (price > balance) {
                                System.out.println("Insufficient balance.");
                            } else {
                                balance -= price;
                            }
                    		break;
                    	case 3:
                    		System.out.println("Your balance : " + balance);
                    		break;
                    }
                } while (select != 4);
                System.out.println("See you again.");
                break;
            } else {
                right--;
                System.out.println("Wrong username or password. Try again.");
                if (right == 0) {
                    System.out.println("Your account has been blocked, please contact the bank.");
                } else {
                    System.out.println("Your Remaining Rights : " + right);
                }
            }
        }
	}

}
