package calculate_power;

import java.util.Scanner;

public class CalculatePower {

	public static void main(String[] args) {
		int x,n,result = 1;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the number : ");
		x = scan.nextInt();
		System.out.print("Enter the power : ");
		n = scan.nextInt();
		
		for(int i = 0; i < n; i++) {
			result *= x;
		}
		
		System.out.println("Result = " + result);
	}

}
