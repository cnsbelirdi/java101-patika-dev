package array_count;

import java.util.*;

public class ArrayCount {

	public static void main(String[] args) {
            int[] array = {10, 20, 20, 10, 10, 20, 5, 20};
            
            System.out.println("The array : " + Arrays.toString(array));
            List<Integer> counterList = new ArrayList<>();
            
            int counter = 0;

            for (int a : array) {
                for (int i : array) {
                    if (a == i) {
                        counter++;
                    }
                }
                if (!(counterList.contains(a)) && counter > 0) {
                    counterList.add(a);
                    System.out.println("The number " + a + " repeated " + counter + " times");
                }
                counter = 0;
            }
	}

}
